package com.emerio.pmo.menuservice.service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.Filters.*;

@Service
public class MongoService{

    @Value("${app.mongodb.host}")
    private String host;

    @Value("${app.mongodb.port}")
    private String port;
    
    @Value("${app.mongodb.database}")
    private String database;

    @Value("${app.mongodb.collection}")
    private String collection;


    private MongoClient getConnection(){
        return MongoClients.create("mongodb://"+this.host+":"+this.port);
    }


    public Document getMenu(String role){

        MongoClient client = this.getConnection();
        MongoDatabase mongodb = client.getDatabase(this.database);
        MongoCollection<Document> collection = mongodb.getCollection(this.collection);

        try{
            Document output = collection.find(eq("role", role)).first();
            output.remove("_id");
            return output;
        } finally{
            client.close();
        }

    }





}