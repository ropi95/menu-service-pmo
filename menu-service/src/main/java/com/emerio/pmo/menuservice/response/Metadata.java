package com.emerio.pmo.menuservice.response;

public class Metadata{
    private String status;
	private String message;
	private Integer code;
	
	
	// public Metadata() {
	// 	super();
	// }
	
	public Metadata( String status, String message, Integer code) {
		this.status = status;
		this.message = message;
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	// public void setStatus(String status) {
	// 	this.status = status;
	// }

	public String getMessage() {
		return message;
	}

	// public void setMessage(String message) {
	// 	this.message = message;
	// }

	public Integer getCode() {
		return code;
	}

	// public void setCode(Integer code) {
	// 	this.code = code;
	// }
}