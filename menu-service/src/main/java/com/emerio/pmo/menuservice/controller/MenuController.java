package com.emerio.pmo.menuservice.controller;

import com.emerio.pmo.menuservice.response.Response;
import com.emerio.pmo.menuservice.service.MongoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class MenuController{

    @Autowired
    MongoService mongoService;

    Response response = new Response();

    @RequestMapping(value = "/menu/{role}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Mono<Response> menu(@PathVariable String role){
        return Mono.just(response.loaded(mongoService.getMenu(role)));
    }


}