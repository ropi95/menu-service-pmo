package com.emerio.pmo.menuservice.response;

public class Response {
	
	/**
	 * 
	 */
	private Metadata meta;
	private Object data;
	
	public Response() {
		super();
	}

	public Response(Object object, String status, String message, Integer  code) {
		this.meta = new Metadata(status, message, code);
		this.data = object;
	}
	
	public Response(String status, String message, Integer  code) {
		this.meta = new Metadata(status, message, code);
	}
	
	public Response loaded(Object object)
	{
		return  new Response(object, "success", "loaded successful", 200);
	}
	
	// public Response create(Object object)
	// {
	// 	return  new Response(object,"success", "create successful", 201);
	// }
	
	// public Response update(Object object)
	// {
	// 	return  new Response(object,"success", "update successful", 201);
	// }
	
	// public Response notFound(String message)
	// {
	// 	return new Response("failed", message, 404);
	// }
	
	// public Response badRequest(String message)
	// {
	// 	return new Response("failed", message, 400);
	// }
	
	// public Response internalServerError()
	// {
	// 	return new Response("failed", "Internal Server Error", 500);
	// }
	
	public Response unauthorized()
	{
		return new Response("failed", "UNAUTHORIZED", 401);
	}
	
	// public Response delete(String message)
	// {
	// 	return new Response("success", message, 200);
	// }

	// getter and setter
	
	public Metadata getMeta() {
		return meta;
	}

	// public void setMeta(Metadata meta) {
	// 	this.meta = meta;
	// }

	public Object getData() {
		return data;
	}

	// public void setData(Object data) {
	// 	this.data = data;
	// }

}
